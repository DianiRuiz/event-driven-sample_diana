using System.Threading.Tasks;
using Cips.Application.Events;

namespace Cips.Application.Common.Interfaces
{
    public interface IEventBus
    {
        Task Publish(EventBase domainEvent);
    }
}